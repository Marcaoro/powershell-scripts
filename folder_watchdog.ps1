$folder = 'C:\Path\to\watch' 

$fsw = New-Object IO.FileSystemWatcher $folder, $filter -Property @{IncludeSubdirectories = $true;NotifyFilter = [IO.NotifyFilters]'FileName, LastWrite'}

Register-ObjectEvent $fsw Changed -SourceIdentifier FileChanged1 -Action { 
$path = $Event.SourceEventArgs.FullPath 
$changeType = $Event.SourceEventArgs.ChangeType 
$timeStamp = $Event.TimeGenerated 
$datestamp = get-date -uformat "%Y-%m-%d@%H-%M-%S" 
$Computer = get-content env:computername 
$Body = " $path on $Computer was $changeType at $timeStamp" 
$PIECES=$path.split("\") 
$newfolder=$PIECES[-2] 
Out-File -FilePath c:\Scripts\config_changes.txt -Append -InputObject " $path on $Computer was $changeType at $timeStamp" 
Send-MailMessage -To "nn.nn@nn.de" -From "no-reply@nn.de" -Subject $Body -SmtpServer "10.20.30.40" -Body "Neue Datei in \\hostname\path\to\watch "
}