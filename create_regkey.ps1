
#Create needed Registry Keys
New-Item -Path "HKLM:\SOFTWARE\Microsoft\InetStp\Configuration"
New-Item -Path "HKLM:\SOFTWARE\Microsoft\InetStp\Configuration\MaxWebConfigFileSizeInKB"

#Modify Registry Key
$registrypath = "HKLM:\SOFTWARE\Microsoft\InetStp\Configuration\"
$Name = "MaxWebConfigFileSizeInKB"
$Value = "3072"
New-ItemProperty -Path $registrypath -Name $Name -Value $Value -PropertyType DWORD -Force