$server = "Enter your SQL Instance"
$user = "sa"
$password = ""

Function Test-SQLConn ()
{
    $connectionString = "Data Source=" + $server+ ";User Id=" + $user + ";Password=" + $password + ";Initial Catalog=master"
        $sqlConn = new-object ("Data.SqlClient.SqlConnection") $connectionString
    trap
    {
        Write-Error "Cannot connect to $Server.";
        continue
    }
    $sqlConn.Open()
    if ($sqlConn.State -eq 'Open')
    {
        $sqlConn.Close();
        "Opened successfully."
    }
}
Test-SQLConn