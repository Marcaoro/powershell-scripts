 
$credential = Get-Credential -Message "Please enter the username and password for authentication"

$sopt = New-PSSessionOption -SkipCACheck -SkipCNCheck

$code = {
    param(
        [string] $Name
    )
    Write-Host "Hello $Name"
}

$codeParams = @( "TestName" )

Invoke-Command -ComputerName XXXXXXXXXXX -Credential $credential -Port 5986 -UseSSL -Authentication Negotiate -ScriptBlock $code -ArgumentList $codeParams -SessionOption $sopt